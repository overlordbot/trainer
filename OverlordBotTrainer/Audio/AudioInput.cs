﻿using NAudio.Wave;
using System;
using System.Collections.Generic;

namespace OverlordBotTrainer.Audio
{
    public class AudioInput
    {
        #region Singleton Definition
        private static volatile AudioInput _instance;
        private static readonly object Lock = new object();

        public static AudioInput Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Lock)
                    {
                        if (_instance == null)
                            _instance = new AudioInput();
                    }
                }

                return _instance;
            }
        }
        #endregion

        #region Instance Definition

        public List<AudioDeviceListItem> InputAudioDevices { get; }

        public AudioDeviceListItem SelectedAudioInput { get; set; }

        private AudioInput()
        {
            InputAudioDevices = BuildAudioInputs();
        }

        private List<AudioDeviceListItem> BuildAudioInputs()
        {
            var inputs = new List<AudioDeviceListItem>();

            if (WaveIn.DeviceCount == 0)
            {
                return inputs;
            }

            inputs.Add(new AudioDeviceListItem()
            {
                Text = "Default Microphone",
                Value = null
            });
            SelectedAudioInput = inputs[0];

            for (var i = 0; i < WaveIn.DeviceCount; i++)
            {

                var item = WaveIn.GetCapabilities(i);
                inputs.Add(new AudioDeviceListItem()
                {
                    Text = item.ProductName,
                    Value = item
                });
            }

            return inputs;
        }

        public int SelectedAudioInputDeviceNumber()
        {
            // Special case for the default
            if (SelectedAudioInput.Value == null)
            {
                return 0;
            }

            var selectedAudioInput = (WaveInCapabilities)SelectedAudioInput.Value;

            for (var i = 0; i < WaveIn.DeviceCount; i++)
            {
                var device = WaveIn.GetCapabilities(i);
                if ((device.ProductName == selectedAudioInput.ProductName) &&
                    (device.ProductGuid == selectedAudioInput.ProductGuid))
                {
                    return i;
                }
            }
            throw new IndexOutOfRangeException("No device number matches selected");
        }
        #endregion
    }

}

