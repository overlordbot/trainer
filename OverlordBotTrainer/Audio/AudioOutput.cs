﻿using NAudio.CoreAudioApi;
using NAudio.Dmo;
using System;
using System.Collections.Generic;
using System.Linq;
using NAudio.Wave;

namespace OverlordBotTrainer.Audio
{
    public class AudioOutput
    {

        #region Singleton Definition
        private static volatile AudioOutput _instance;
        private static readonly object Lock = new object();

        public static AudioOutput Instance
        {
            get
            {
                if (_instance != null) return _instance;
                lock (Lock)
                {
                    if (_instance == null)
                        _instance = new AudioOutput();
                }

                return _instance;
            }
        }
        #endregion

        #region Instance Definition

        public List<AudioDeviceListItem> OutputAudioDevices { get; }
        public AudioDeviceListItem SelectedAudioOutput { get; set; }

        // Version of Windows without bundled multimedia stuff as part of European anti-trust settlement
        // https://support.microsoft.com/en-us/help/11529/what-is-a-windows-7-n-edition
        public bool WindowsN { get; set; }

        private AudioOutput()
        {
            WindowsN = DetectWindowsN();
            OutputAudioDevices = BuildNormalAudioOutputs();
        }

        private List<AudioDeviceListItem> BuildNormalAudioOutputs()
        {
            return BuildAudioOutputs("Default Speakers");
        }

        private List<AudioDeviceListItem> BuildAudioOutputs(string defaultItemText)
        {
            var outputs = new List<AudioDeviceListItem>
            {
                new AudioDeviceListItem()
                {
                    Text = defaultItemText,
                    Value = new MMDeviceEnumerator().GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia)
                }
            };

            SelectedAudioOutput = outputs[0];

            var enumerator = new MMDeviceEnumerator();
            var outputDeviceList = enumerator.EnumerateAudioEndPoints(DataFlow.Render, DeviceState.Active);

            outputs.AddRange(outputDeviceList.Select(device => new AudioDeviceListItem {Text = device.FriendlyName, Value = device}));

            return outputs;
        }

        private static bool DetectWindowsN()
        {
            try
            {
                var dmoResampler = new DmoResampler();
                dmoResampler.Dispose();
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }

        #endregion
    }
}
