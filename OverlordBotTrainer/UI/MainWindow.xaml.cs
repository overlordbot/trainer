﻿using System.Text.RegularExpressions;
using System.Windows;

namespace OverlordBotTrainer.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly Regex CallsignPattern = new Regex("([a-zA-Z]+ ?)");

        public MainWindow()
        {
            InitializeComponent();
        }

        private void ConfirmCallsign(object sender, RoutedEventArgs e)
        {
            var recordingWindow = new RecordingWindow(CallsignTextBox.Text.Trim().ToLower());
            Application.Current.MainWindow = recordingWindow;
            Close();
            recordingWindow.Show();
        }

        private void CallsignUpdated(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            CallsignConfirmButton.IsEnabled = CallsignPattern.Matches(CallsignTextBox.Text).Count >= 3;
        }
    }
}
